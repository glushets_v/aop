﻿using System;
using System.Web.Helpers;
using AOP.Logger;
using PostSharp.Aspects;

namespace AOP.CodeRewriting
{
    [Serializable]
    public class LoggerAspect : OnMethodBoundaryAspect
    {
        private ILogger _logger;

        public LoggerAspect()
        {
            _logger = new Logger.Logger();
        }

        public override void OnEntry(MethodExecutionArgs args)
        {
            var methodName = args.Method.Name;
            var inputParams = Json.Encode(args.Arguments);

            var startMessage = $"{DateTime.Now} {methodName} parameters: {inputParams}";

            _logger.Trace(startMessage);

            args.FlowBehavior=FlowBehavior.Default;
        }

        public override void OnSuccess(MethodExecutionArgs args)
        {
            var returnValue = Json.Encode(args.ReturnValue);

            var endMessage = $"Result value: {returnValue}";

            _logger.Trace(endMessage);
        }
    }
}
