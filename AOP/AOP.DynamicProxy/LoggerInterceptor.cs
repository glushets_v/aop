﻿using System;
using System.Web.Helpers;
using AOP.Logger;
using Castle.DynamicProxy;

namespace AOP.DynamicProxy
{
    public class LoggerInterceptor : IInterceptor
    {
        private ILogger _logger;
        
        public LoggerInterceptor()
        {
            _logger = new Logger.Logger();
        }

        public void Intercept(IInvocation invocation)
        {
            var methodName = invocation.Method.Name;
            var inputParams = Json.Encode(invocation.Arguments);

            var startMessage = $"{DateTime.Now} {methodName} parameters: {inputParams}";

            _logger.Trace(startMessage);

            invocation.Proceed();

            var returnValue = Json.Encode(invocation.ReturnValue);

            var endMessage = $"Result value: {returnValue}";

            _logger.Trace(endMessage);

            // var t = invocation.Arguments; - все аргументы метода
            // invocation.Proceed(); - продолжить выполнение оригинального метода
            // invocation.ReturnValue - видимо результат оригинального метода
        }
    }
}