﻿namespace AOP.Logger
{
    public interface ILogger
    {
        void Trace(string message);
    }
}
