﻿using System;
using System.IO;
using System.Reflection;
using System.Text;

namespace AOP.Logger
{
    [Serializable]
    public class Logger : ILogger
    {
        public void Trace(string message)
        {
            var dirPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase);
            var path = Path.Combine(dirPath.Substring(6), "log.txt");

            if (!File.Exists(path))
            {
                File.WriteAllText(path, message, Encoding.UTF8);
            }
            else
            {
                using (StreamWriter sw = File.AppendText(path))
                {
                    sw.WriteLine(message);
                }
            }
        }
    }
}