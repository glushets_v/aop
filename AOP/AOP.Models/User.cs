﻿using System;

namespace AOP.Models
{
    [Serializable]
    public class User
    {
        public string Name { get; set; }

        public int Age { get; set; }
    }
}
