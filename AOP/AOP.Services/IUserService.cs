﻿using System.Collections.Generic;
using AOP.Models;

namespace AOP.Services
{
    public interface IUserService
    {
        List<User> GetAllUsers();
        User GetUser(string name);
    }
}
