﻿using System;
using System.Collections.Generic;
using System.Linq;
using AOP.Models;

namespace AOP.Services
{
    public class UserService : IUserService
    {
        private List<User> _storageUsers;

        public UserService()
        {
            _storageUsers = new List<User>
            {
                new User
                {
                    Age = 30,
                    Name = "Vasiliy"
                },
                new User
                {
                    Age = 32,
                    Name = "Anastasiya"
                }
            };
        }

        public List<User> GetAllUsers()
        {
            return _storageUsers;
        }

        public User GetUser(string name)
        {
            return _storageUsers.FirstOrDefault(x => x.Name.Equals(name, StringComparison.OrdinalIgnoreCase));
        }
    }
}
