﻿using System.Web.Mvc;
using AOP.CodeRewriting;
using AOP.Services;

namespace AOP.Web.Controllers
{
    public class CodeRewritingController : Controller
    {
        private IUserService _userService;

        public CodeRewritingController()
        {
            _userService = new UserService();
        }

        // GET: CodeRewriting

        public ActionResult Index()
        {
            return View();
        }

        [LoggerAspect]
        [HttpGet]
        public ActionResult CodeRewriting(string name)
        {
            return View(_userService.GetUser(name));
        }

    }
}